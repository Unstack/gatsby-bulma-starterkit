import React from 'react'
import Link from 'gatsby-link'

const RoutedNavbar = (props) => (
  <div className="navbar">
    <div className="navbar-brand">
      <Link to='/' classname='navbar-item'>{props.siteTitle}</Link>
      <div/>
    </div>
    <div className="navbar-menu">
      <div className="navbar-item">
        <Link to="/about" className="navbar-item">About</Link>
      </div> 
    </div>
  </div>
)

export default RoutedNavbar
